use crate::util::BuilderHelper;
use glib::clone;
use gtk::{Box, Button, ButtonExt, WidgetExt};
use libhandy::{Leaflet, LeafletExt};
use log::debug;
use parking_lot::RwLock;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct ResponsiveLayout {
    pub state: Arc<RwLock<ResponsiveState>>,
    pub left_back_button: Button,
    pub right_back_button: Button,
    pub major_leaflet: Leaflet,
    pub minor_leaflet: Leaflet,
    pub sidebar_box: Box,
    pub article_list_box: Box,
    pub article_view_box: Box,
}

impl ResponsiveLayout {
    pub fn new(builder: &BuilderHelper) -> Arc<ResponsiveLayout> {
        let state = Arc::new(RwLock::new(ResponsiveState::new()));

        let minor_leaflet = builder.get::<Leaflet>("minor_leaflet");
        let major_leaflet = builder.get::<Leaflet>("major_leaflet");
        let left_back_button = builder.get::<Button>("left_back_button");
        let right_back_button = builder.get::<Button>("right_back_button");
        let sidebar_box = builder.get::<Box>("feedlist_box");
        let article_list_box = builder.get::<Box>("articlelist_box");
        let article_view_box = builder.get::<Box>("articleview_box");
        let layout = ResponsiveLayout {
            state,
            left_back_button,
            right_back_button,
            major_leaflet,
            minor_leaflet,
            sidebar_box,
            article_list_box,
            article_view_box,
        };
        let layout = Arc::new(layout);
        Self::setup_signals(&layout);
        layout
    }

    fn setup_signals(layout: &Arc<ResponsiveLayout>) {
        let major_leaflet = layout.major_leaflet.clone();
        let minor_leaflet = layout.minor_leaflet.clone();

        layout.major_leaflet.connect_property_folded_notify(clone!(
            @weak layout => @default-panic, move |_leaflet|
        {
            if minor_leaflet.get_folded() {
                debug!("Widget: Minor Leaflet folded");
                layout.state.write().minor_leaflet_folded = true;
                Self::process_state_change(&layout);
            }
            debug!("Widget: Major Leaflet folded");
            layout.state.write().major_leaflet_folded = true;
            Self::process_state_change(&layout);
        }));

        layout.minor_leaflet.connect_property_folded_notify(clone!(
            @weak layout => @default-panic, move |_leaflet|
        {
            if !major_leaflet.get_folded() {
                return;
            }
            layout.state.write().minor_leaflet_folded = true;
            Self::process_state_change(&layout);
        }));

        // swipe back should be handled the same way as back button press
        layout.minor_leaflet.connect_property_visible_child_name_notify(
            clone!(@weak layout => @default-panic, move |leaflet| {
                if leaflet.get_visible_child_name().map(|s| s.as_str().to_owned()) == Some("feedlist_box".into()) {
                    layout.state.write().left_button_clicked = true;
                    Self::process_state_change(&layout);
                }
            }),
        );

        layout
            .left_back_button
            .connect_clicked(clone!(@weak layout => @default-panic, move |_button| {
                layout.state.write().left_button_clicked = true;
                Self::process_state_change(&layout);
            }));

        layout
            .right_back_button
            .connect_clicked(clone!(@weak layout => @default-panic, move |_button| {
                layout.state.write().right_button_clicked = true;
                Self::process_state_change(&layout);
            }));
    }

    pub fn process_state_change(&self) {
        if self.state.read().major_leaflet_folded {
            // article view (dis)appeared
            if !self.major_leaflet.get_folded() {
                self.right_back_button.set_visible(false);
                self.left_back_button.set_visible(false);
                self.major_leaflet.set_visible_child(&self.minor_leaflet);
                self.minor_leaflet.set_visible_child(&self.sidebar_box);
            }

            self.state.write().major_leaflet_folded = false;
            return;
        }

        if self.state.read().minor_leaflet_folded {
            // article list (dis)appeared
            if !self.minor_leaflet.get_folded() {
                self.left_back_button.set_visible(false);
                self.minor_leaflet.set_visible_child(&self.sidebar_box);
            }

            self.state.write().minor_leaflet_folded = false;
            return;
        }

        if self.state.read().left_button_clicked {
            // left back
            self.minor_leaflet.set_visible_child(&self.sidebar_box);

            self.state.write().left_button_clicked = false;
            return;
        }

        if self.state.read().right_button_clicked {
            // right back
            //self.minor_leaflet.set_visible_child(&self.article_list_box);
            self.major_leaflet.set_visible_child(&self.minor_leaflet);
            self.right_back_button.set_visible(false);

            self.state.write().right_button_clicked = false;
            return;
        }

        if self.state.read().major_leaflet_selected {
            // article selected
            if self.major_leaflet.get_folded() {
                self.major_leaflet.set_visible_child(&self.article_view_box);
                self.right_back_button.set_visible(true);
            }

            self.state.write().major_leaflet_selected = false;
            return;
        }

        if self.state.read().minor_leaflet_selected {
            // sidebar selected
            if self.minor_leaflet.get_folded() {
                self.minor_leaflet.set_visible_child(&self.article_list_box);
                self.left_back_button.set_visible(true);
            }

            self.state.write().minor_leaflet_selected = false;
            return;
        }
    }
}

#[derive(Clone, Debug)]
pub struct ResponsiveState {
    pub left_button_clicked: bool,
    pub right_button_clicked: bool,
    pub major_leaflet_selected: bool,
    pub major_leaflet_folded: bool,
    pub minor_leaflet_selected: bool,
    pub minor_leaflet_folded: bool,
}

impl ResponsiveState {
    pub fn new() -> Self {
        ResponsiveState {
            left_button_clicked: false,
            right_button_clicked: false,
            major_leaflet_selected: false,
            major_leaflet_folded: false,
            minor_leaflet_selected: false,
            minor_leaflet_folded: false,
        }
    }
}
